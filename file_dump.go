package main

import (
        "bufio"
        "fmt"
        "log"
        "os"
        "path"
        "runtime"
)

var data []byte
var err error
var file string
var digits int = 8

func main() {

        if isPipe() {
                scanner := bufio.NewScanner(os.Stdin)
                for scanner.Scan() {
                        data = append(data, scanner.Bytes()...)
                }

                err = scanner.Err()
                er(err)
        }
        fmt.Println(data)

        if len(os.Args) != 2 && data == nil {
                log.Fatal("Must Specify File.")
                return
        }

        if len(os.Args) != 1 {
                args := os.Args[1:]

                for i := 0; i < len(args); i++ {
                        if args[i] == "--help" || args[i] == "-h" {
                                fmt.Print("-h | --help\tPrint this help message.\n")
                                fmt.Print("FILENAME\tPath to file to dump.\n")
                                fmt.Print("<STDIN>\tAccepts file from standard input.\n")
                                os.Exit(0)
                        } else {
                                _, err = os.Stat(args[i])
                                file = args[i]
                        }
                }
        }

        if data == nil {
                data, err = os.ReadFile(file)
                er(err)
        }

        for i := 0; i < len(data); i++ {

                if i == 0 {
                        fmt.Printf("0x%08X\t", i)
                }

                fmt.Printf("%08b", data[i])
                fmt.Printf(" ")

                if (i+1)%digits == 0 && i != 0 {
                        fmt.Printf("\t")
                        for j := i - digits + 1; j <= i; j++ {
                                fmt.Printf("%02X", data[j])
                                fmt.Printf(" ")
                        }
                        fmt.Printf("\t")
                        for k := i - digits + 1; k <= i; k++ {
                                fmt.Printf("%s", string(fmt.Sprintf("%q", string(rune(data[k])))[1:len(fmt.Sprintf("%q", string(rune(data[k]))))-1]))
                                if len(string(fmt.Sprintf("%q", string(rune(data[k])))[1:len(fmt.Sprintf("%q", string(rune(data[k]))))-1])) == 1 {
                                        fmt.Printf("  ")
                                } else {
                                        fmt.Printf(" ")
                                }
                        }

                        fmt.Printf("\n")
                        fmt.Printf("0x%08X\t", i+1)
                }
        }
}

func er(err error) {
        pc, file, line, ok := runtime.Caller(1)
        if !ok {
                log.Fatal("error getting caller function\n")
                os.Exit(3)
        }

        if err != nil {
                log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
        }
}

func isPipe() bool {
        fileInfo, _ := os.Stdin.Stat()
        return fileInfo.Mode()&os.ModeCharDevice == 0
}